import { CodeLensProvider, TextDocument, CodeLens, Range, Position, Command } from "vscode";
import packageJson = require('package-json');

// compare x.x.x with y.y.y
// a > b ?
export const compareVersion = (a: string, b: string): boolean =>
    parseInt(a.replace(/\./g, '000'), 10) > parseInt(b.replace(/\./g, '000'), 10);

export class VersionsCodeLensProvider implements CodeLensProvider {
    provideCodeLenses(document: TextDocument): Promise<CodeLens[]> {
        let sourceCode = document.getText()
        const results: Promise<CodeLens[]>[] = [];
        const sourceCodeArr = sourceCode.split('\n');
        let regexStart = /.*(devDependencies|dependencies).*/
        let regexEnd = /},?/
        let isActive = false;

        for (let lineNum = 0; lineNum < sourceCodeArr.length; lineNum++) {
            const line = sourceCodeArr[lineNum];
            const matchStart = line.match(regexStart);
            const matchEnd = line.match(regexEnd);
            // on
            if (matchStart && matchStart.index !== undefined) {
                isActive = true;
                continue;
            }
            // off
            if (matchEnd && matchEnd.index !== undefined) {
                isActive = false;
                continue;
            }
            if (isActive) {
                results.push(this.getCodeLenseForLine(lineNum, line));
            }
        }
        return Promise.all(results)
            .then(results => results.flat());
    }

    getCodeLenseForLine(lineNum: number, line: string): Promise<CodeLens[]> {
        const parts = line
            .split(':')
            .map(i => i.trim().replace(/["\^]/g, ''));
        const [packageName, packageVersion] = parts;

        return packageJson(packageName)
            .then(result => {
                let recentPackageVersion = result.version as string;
                const isRecentBigger = compareVersion(recentPackageVersion, packageVersion)
                const isCurrentBigger = compareVersion(packageVersion, recentPackageVersion)
                if (isRecentBigger || isCurrentBigger) {
                    let range = new Range(
                        new Position(lineNum, 0),
                        new Position(lineNum, recentPackageVersion.length - 1)
                    )
                    let c: Command = {
                        command: "extension.fixVersion",
                        title: recentPackageVersion,
                        arguments: [
                            lineNum,
                            line,
                            recentPackageVersion
                        ]
                    };
                    return [new CodeLens(range, c)];
                }
                return [];
            })
    }
}