import * as vscode from 'vscode';
import {
	ExtensionContext,
	languages,
	window
} from "vscode";
import { VersionsCodeLensProvider } from './VersionsCodeLensProvider'

export function activate(context: ExtensionContext) {
	let fixCommand = vscode.commands.registerCommand('extension.fixVersion', fixVersion);

	// Register our CodeLens provider
	let codeLensProvider = languages.registerCodeLensProvider(
		{
			language: "json",
			scheme: "file"
		},
		new VersionsCodeLensProvider()
	);
	context.subscriptions.push(fixCommand);
	context.subscriptions.push(codeLensProvider);
}
// [line number, full line, correct version]
export function fixVersion(...args: string[]) {
	const lineNum = parseInt(args[0], 10);
	const line = args[1];
	const correctVersion = args[2];
	console.log(`Command for: ${args}`);
	let range = new vscode.Range(
		new vscode.Position(lineNum, line.lastIndexOf(':') + 1),
		new vscode.Position(lineNum, line.length)
	);
	let comma = line.indexOf(',') > -1 ? ',' : '';
	let snippet = new vscode.SnippetString(` "${correctVersion}"${comma}`);
	if (window.activeTextEditor) {
		window.activeTextEditor.insertSnippet(snippet, range);
	}
}
